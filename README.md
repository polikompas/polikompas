# Polikompas

[![pipeline status](https://gitlab.com/Quintasan/polikompas/badges/master/pipeline.svg)](https://gitlab.com/Quintasan/polikompas/-/commits/master)  [![coverage report](https://gitlab.com/Quintasan/polikompas/badges/master/coverage.svg)](https://gitlab.com/Quintasan/polikompas/-/commits/master)

## Requirements

| Requirement | Version |
|-------------|---------|
| Ruby        | 2.7.2   |
| PostgreSQL  | 12      |
| Redis       | 6       |

## Development

### Requirements

1. [Docker](https://docs.docker.com/get-docker/)
2. [Docker Compose](https://docs.docker.com/compose/install/)
3. [dip](https://github.com/bibendi/dip#installation) - this is optional but recommended

### Setup

1. Clone the source code
2. `bin/setup`
3. `dip rails s`
4. Visit http://localhost:3000
