class AddAreaToLawProposal < ActiveRecord::Migration[5.2]
  def change
    add_column :law_proposals, :area, :string, null: false, default: "unknown"
  end
end
